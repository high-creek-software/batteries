package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/high-creek-software/batteries/internal/item"
)

func main() {
	log.Println("Starting batteriesd")
	db, err := sql.Open("sqlite3", "batteries.db")
	if err != nil {
		log.Fatal(err)
	}

	itemRepo := item.NewRepoSql(db)

	log.Println(itemRepo)

	// b := owner.Owner{ID: "br", Name: "b"}
	// now := time.Now()
	// itm := item.Item{ID: "litebrite",
	// 	Name:        "Lite Brite",
	// 	ImagePath:   "",
	// 	DateChanged: &now,
	// 	Count:       3,
	// 	Owner:       b,
	// }

	// strErr := itemRepo.Store(itm)
	// if strErr != nil {
	// 	log.Println(strErr)
	// }

	manager := Manager{itemRepo: itemRepo}

	mux := http.NewServeMux()

	mux.HandleFunc("/item", manager.ListItems)
	mux.HandleFunc("/owner", manager.ListOwners)

	http.ListenAndServe(":8080", mux)
}

type Manager struct {
	itemRepo item.Repo
}

func (m Manager) ListItems(w http.ResponseWriter, r *http.Request) {
	items, err := m.itemRepo.List()
	if err != nil {
		http.Error(w, "error loading items", http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	data, _ := json.Marshal(items)
	w.Write(data)

}

func (m Manager) ListOwners(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "all owners")
}
