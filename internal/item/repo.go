package item

import (
	"database/sql"
	"encoding/json"
	"log"

	"github.com/dgraph-io/badger/v3"
)

type Repo interface {
	Store(item Item) error
	List() ([]Item, error)
}

type RepoSql struct {
	db *sql.DB
}

func NewRepoSql(db *sql.DB) Repo {
	repo := &RepoSql{db: db}

	create := `create table if not exists item(
		id varchar(25) primary key,
		name text,
		image_path text,
		date_changed datetime,
		count integer,
		owner text
	);`

	_, err := repo.db.Exec(create)

	if err != nil {
		log.Fatalln(err)
	}

	return repo
}

func (rs *RepoSql) Store(item Item) error {
	ins := `INSERT INTO item(id, name, image_path, count, owner) VALUES(?, ?, ?, ?, ?);`
	bs, err := json.Marshal(item.Owner)
	if err != nil {
		return err
	}
	_, insErr := rs.db.Exec(ins, item.ID, item.Name, item.ImagePath, item.Count, string(bs))

	return insErr
}

func (rs *RepoSql) List() ([]Item, error) {

	liteBrite := Item{ID: "lite-brite", Name: "Lite Brite", Count: 3}
	audioRecorder := Item{ID: "audio", Name: "Recorder", Count: 5}

	var items []Item
	items = append(items, liteBrite)
	items = append(items, audioRecorder)

	return items, nil
}

type RepoBadger struct {
	db *badger.DB
}

func NewRepoBader(db *sql.DB) Repo {
	return &RepoSql{db: db}
}

func (rs *RepoBadger) Store(item Item) error {
	return nil
}

func (rs *RepoBadger) List() ([]Item, error) {
	return nil, nil
}
