package item

import (
	"time"

	"gitlab.com/high-creek-software/batteries/internal/owner"
)

type Item struct {
	ID          string      `json:"id"`
	Name        string      `json:"name"`
	ImagePath   string      `json:"imagePath"`
	DateChanged *time.Time  `json:"dateChanged"`
	Count       int         `json:"count"`
	Owner       owner.Owner `json:"owner"`
}
