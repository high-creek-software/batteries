package owner

type Owner struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
